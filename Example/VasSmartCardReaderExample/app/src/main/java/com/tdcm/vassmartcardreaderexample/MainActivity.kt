package com.tdcm.vassmartcardreaderexample

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.trueit.vas.readcard.vascardwrapper.smartcardreader.TrueSmartCardReader
import com.trueit.vas.readcard.vascardwrapper.smartcardreader.exception.Error
import com.trueit.vas.readcard.vascardwrapper.smartcardreader.page.setting.SmartCardSettingActivity
import com.trueit.vassmartcardreader.exception.VasException
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        findViewById<View>(R.id.checkStatusButton)?.setOnClickListener {
            checkStatus()
        }

        findViewById<View>(R.id.readThaiIdButton)?.setOnClickListener {
            readThaiId()
        }

        findViewById<View>(R.id.readCreditCardButton)?.setOnClickListener {
            readCreditCard()
        }

        findViewById<View>(R.id.gotoSettingButton)?.setOnClickListener {
            gotoSettingPage()
        }
    }

    private fun readCreditCard() {
        TrueSmartCardReader.getCardReaderService()
            .connect()
            .flatMap { service -> service.readCreditCard() }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                showResult(it.toString())
            }, {
                if (it is VasException) {
                    showResult("${it.code} - ${it.message}")
                    //This error come from sdk
                    when (it.code) {
                        //import com.trueit.vas.readcard.vascardwrapper.smartcardreader.exception.Error
                        Error.BLUETOOTH_DEVICE_NOT_FOUND_ERROR_CODE -> {
                            //TODO handle device not found error
                            Log.e("Result", "BLUETOOTH_DEVICE_NOT_FOUND_ERROR_CODE")
                        }
                        Error.CARD_NOT_FOUND_ERROR_CODE -> {
                            //TODO handle card not found error
                            Log.e("Result", "CARD_NOT_FOUND_ERROR_CODE")
                        }
                        else -> {
                            //TODO handle another error
                        }
                    }
                } else {
                    //TODO handle unknown error
                    showResult(it.message)
                }
            })
    }

    private fun readThaiId() {
    TrueSmartCardReader.getCardReaderService()
      .connect()
      .flatMap { service -> service.readThaiIDAll() }
      .subscribeOn(Schedulers.io())
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe({
        showResult(it.toString())

      }, {
        if (it is VasException) {
          showResult("${it.code} - ${it.message}")
          //This error come from sdk
          when (it.code) {
            //import com.trueit.vas.readcard.vascardwrapper.smartcardreader.exception.Error
            Error.BLUETOOTH_DEVICE_NOT_FOUND_ERROR_CODE -> {
              //TODO handle device not found error
              Log.e("Result", "BLUETOOTH_DEVICE_NOT_FOUND_ERROR_CODE")
            }
            Error.CARD_NOT_FOUND_ERROR_CODE -> {
              //TODO handle card not found error
              Log.e("Result", "CARD_NOT_FOUND_ERROR_CODE")
            }
            else -> {
              //TODO handle another error
            }
          }
        } else {
          //TODO handle unknown error
          showResult(it.message)
        }
      })
    }

    private fun gotoSettingPage() {
        startActivity(Intent(this, SmartCardSettingActivity::class.java))
    }

    private fun checkStatus() {
        val cardReaderService = TrueSmartCardReader.getCardReaderService()
        val deviceType = cardReaderService.getDeviceType()

        val stringBuilder = StringBuilder()
            .append("$deviceType Service state ${cardReaderService.getServiceState()}")
            .append('\n')
            .append("$deviceType Device state ${cardReaderService.getDeviceState()}")
            .append('\n')
            .append("$deviceType Card state ${cardReaderService.getCardState()}")

        showResult(stringBuilder.toString())
    }

    private fun showResult(result: String?) {
        findViewById<TextView>(R.id.result)?.text = result
    }
}