# VasSmartCardRead V2.6.0

# วิธีการติดตั้ง
1. เพิ่มโค้ดใน build.gradle ของแอพพลิเคชั่นโมดูล เช่น app/build.gradle

```
repositories{
  ...
  
  maven {
    ...
    mavenCentral()
    maven { url "https://jitpack.io" }
  }
}

dependencies {
  ...

  implementation 'io.reactivex.rxjava2:rxjava:2.2.1'
  implementation 'io.reactivex.rxjava2:rxandroid:2.1.0'

  implementation 'com.gitlab.ruchupong:vassmartcardreader:2.6.0'
}
```

3. ใส่ Secret Key ใน AndroidManifest.xml
```
<application>
  ...

  <meta-data
    android:name="com.trueit.vas.readcard.vascardwrapper.smartcardreader.SecretKey"
    android:value="Your secret key"/>


</application>
```

# วิธีเรียกใช้งาน
1. Check Status

```
val isAvailable = TrueSmartCardReader.isAvailable
```

2. Read Thai ID Card

```
TrueSmartCardReader.getCardReaderService()
      .connect()
      .flatMap { service -> service.readThaiIDAll() }
      .subscribeOn(Schedulers.io())
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe({ result : JSONObject ->
        //TODO Handle result
      }, {
        if (it is VasException) {
          //This error come from sdk
          when (it.code) {
            //import com.trueit.vas.readcard.vascardwrapper.smartcardreader.exception.Error
            Error.BLUETOOTH_DEVICE_NOT_FOUND_ERROR_CODE -> {
              //TODO handle device not found error
              Log.e("Result", "BLUETOOTH_DEVICE_NOT_FOUND_ERROR_CODE")
            }
            Error.CARD_NOT_FOUND_ERROR_CODE -> {
              //TODO handle card not found error
              Log.e("Result", "CARD_NOT_FOUND_ERROR_CODE")
            }
            else -> {
              //TODO handle another error
            }
          }
        } else {
          //TODO handle unknown error
        }
      })
```

3. Read Credit Card
```
TrueSmartCardReader.getCardReaderService()
      .connect()
      .flatMap { service -> service.readCreditCard() }
      .subscribeOn(Schedulers.io())
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe({ result : JSONObject ->
        //TODO Handle result
      }, {
        if (it is VasException) {
          //This error come from sdk
          when (it.code) {
            //import com.trueit.vas.readcard.vascardwrapper.smartcardreader.exception.Error
            Error.BLUETOOTH_DEVICE_NOT_FOUND_ERROR_CODE -> {
              //TODO handle device not found error
              Log.e("Result", "BLUETOOTH_DEVICE_NOT_FOUND_ERROR_CODE")
            }
            Error.CARD_NOT_FOUND_ERROR_CODE -> {
              //TODO handle card not found error
              Log.e("Result", "CARD_NOT_FOUND_ERROR_CODE")
            }
            else -> {
              //TODO handle another error
            }
          }
        } else {
          //TODO handle unknown error
        }
      })
```